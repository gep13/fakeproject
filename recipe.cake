#load nuget:?package=Chocolatey.Cake.Recipe&version=0.28.4

///////////////////////////////////////////////////////////////////////////////
// RECIPE SCRIPT
///////////////////////////////////////////////////////////////////////////////

Environment.SetVariableNames();

BuildParameters.SetParameters(context: Context,
                            buildSystem: BuildSystem,
                            sourceDirectoryPath: "./src",
                            solutionFilePath: "./src/fakeproject.sln",
                            solutionDirectoryPath: "./src/fakeproject",
                            resharperSettingsFileName: "fakeproject.sln.DotSettings",
                            title: "Fake Project",
                            repositoryOwner: "gep13",
                            repositoryName: "fakeproject",
                            productName: "Fake Project",
                            productDescription: "The Ultimate Fake Project.",
                            productCopyright: string.Format("Copyright © 2025 - {0} gep13.", DateTime.Now.Year),
                            shouldStrongNameSignDependentAssemblies: true,
                            strongNameDependentAssembliesInputPath: string.Format("{0}{1}", ((FilePath)("./src")).FullPath, "\\packages\\RestSharp*"),
                            treatWarningsAsErrors: false,
                            shouldRunILMerge: false,
                            repositoryHostedInGitLab: true);

ToolSettings.SetToolSettings(context: Context);

BuildParameters.PrintParameters(Context);

Build.Run();
